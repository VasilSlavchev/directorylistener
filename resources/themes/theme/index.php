<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Vaseto.tk - Directory listing of <?php echo $lister->getListedPath(); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Vaseto.tk, Vasil Slavchev, Abfinden.ucoz.com - http://abfinden.ucoz.com"/>
    <meta name="keywords" content="Vaseto.tk, Vasil Slavchev, Abfinden.ucoz.com - http://abfinden.ucoz.com">
    <link rel="shortcut icon" href="<?php echo THEMEPATH; ?>/images/folder.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo THEMEPATH; ?>/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo THEMEPATH; ?>/style.css" />
    <script type="text/javascript" src="<?php echo THEMEPATH; ?>/jquery1.7.min.js"></script>
    <script type="text/javascript" src="<?php echo THEMEPATH; ?>/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo THEMEPATH; ?>/directorylister.js"></script>
    <meta charset="utf-8">
</head>

<body>

<div class="container">

    <div class="breadcrumb-wrapper">
        <ul class="breadcrumb">
            <?php $divider = FALSE; foreach($lister->listBreadcrumbs() as $breadcrumb): ?>
            <li>
                <?php if ($divider): ?>
                    <span class="divider">/</span>
                <?php endif; ?>
                <a href="<?php echo $breadcrumb['link']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
            <?php $divider = TRUE; endforeach; ?>
            <li class="floatRight" style="display: hidden;">
                <a href="#" id="pageTopLink">Back to top</a>
            </li>
        </ul>
    </div>

    <?php if($lister->getSystemMessages()): ?>
        <?php foreach ($lister->getSystemMessages() as $message): ?>
            <div class="alert alert-<?php echo $message['type']; ?>">
                <?php echo $message['text']; ?>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

   <div id="header" class="clearfix">
        <span class="fileName">File list:</span>
        <span class="fileSize">Size</span>
        <span class="fileModTime">Last Modified</span>
    </div>

    <ul id="directoryListing">
    <?php $x = 1; foreach($lister->listDirectory() as $name => $fileInfo): ?>
        <li class="<?php echo $x %2 == 0 ? 'even' : 'odd'; ?>">
            <a href="<?php echo $fileInfo['file_path']; ?>" class="clearfix">
                <span class="fileName">
                    <i class="<?php echo $fileInfo['icon_class']; ?>">&nbsp;</i>
                    <?php echo $name; ?>
                </span>
				<span class="fileSize"><?php echo $fileInfo['file_size']; ?></span>
                <span class="fileModTime"><?php echo $fileInfo['mod_time']; ?></span>
            </a>
        </li>
    <?php $x++; endforeach; ?>
    </ul>

    <div class="footer">
        </div>Powered by <a href="http://www.vaseto.tk"> Vaseto.Tk </a> and <a href="http://www.abfinden.ucoz.com"> Abfinden.ucoz.com </a>
        <p>
            <!-- NACHALO NA TYXO.BG BROYACH -->
            <script type="text/javascript">
            <!--
            d=document;d.write('<a href="http://www.tyxo.bg/?162434" title="Tyxo.bg counter"><img width="88" height="31" border="0" alt="Tyxo.bg counter" src="'+location.protocol+'//cnt.tyxo.bg/162434?rnd='+Math.round(Math.random()*2147483647));
            d.write('&sp='+screen.width+'x'+screen.height+'&r='+escape(d.referrer)+'"></a>');
            //-->
            </script><noscript><a href="http://www.tyxo.bg/?162434" title="Tyxo.bg counter"><img src="http://cnt.tyxo.bg/162434" width="88" height="31" border="0" alt="Tyxo.bg counter" /></a></noscript>
            <!-- KRAI NA TYXO.BG BROYACH -->
			
			
			<!-- NACHALO NA TYXO.BG BROYACH  !!!!!NEW!!!!-->
				<script type="text/javascript">
				<!--
				d=document;d.write('<a href="http://www.tyxo.bg/?164125" title="Tyxo.bg counter"><img width="88" height="31" border="0" alt="Tyxo.bg counter" src="'+location.protocol+'//cnt.tyxo.bg/164125?rnd='+Math.round(Math.random()*2147483647));
				d.write('&sp='+screen.width+'x'+screen.height+'&r='+escape(d.referrer)+'"></a>');
				//-->
				</script><noscript><a href="http://www.tyxo.bg/?164125" title="Tyxo.bg counter"><img src="http://cnt.tyxo.bg/164125" width="88" height="31" border="0" alt="Tyxo.bg counter" /></a></noscript>
			<!-- KRAI NA TYXO.BG BROYACH -->
				
				<!-- google analitics for - 95.158.167.103 -->
				<script>
				  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				  ga('create', 'UA-47744684-7', 'auto');
				  ga('send', 'pageview');

				</script>
				<!-- google analitics for - 95.158.167.103 -->
				
				
				    <script>
					  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					  ga('create', 'UA-47744684-5', '80.72.93.131');
					  ga('require', 'displayfeatures');
					  ga('send', 'pageview');
					</script>

					<script type="text/javascript">
						var _gaq = _gaq || [];
						_gaq.push(['_setAccount', 'UA-47744684-5']);
						_gaq.push(['_trackPageview']);
						(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

						ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
						})();
					</script>
        </p>
    </div>

</div>

</body>
</html>
